# ING-Invoicing app

## Installation

- Clone this repo
- Install dependencies via `npm install`

## Development

In order to run ionic dev-environment commands, use `npx` command. E.g. `npx ionic serve` - this allow you to run latest ionic dev-environment without affecting globally-installed `ionic` command.

For development run `npm start`

## Useful links

- [API Endpoint](https://ing-invoicing.herokuapp.com/)
- [API Docs](https://ing-invoicing.herokuapp.com/explorer)
