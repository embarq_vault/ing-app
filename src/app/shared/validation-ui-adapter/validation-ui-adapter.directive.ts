import { Directive, ContentChild, EventEmitter, OnDestroy, AfterContentInit } from '@angular/core';
import { Input, Label, Datetime } from '@ionic/angular';
import { FormControlName } from '@angular/forms';
import { tap, takeUntil } from 'rxjs/operators';

@Directive({
  selector: '[ingValidationUiAdapter]'
})
export class ValidationUiAdapterDirective
implements OnDestroy, AfterContentInit {
  @ContentChild(Input)
  public inputRef: Input;

  @ContentChild(Datetime)
  public dateTimeRef: Datetime;

  @ContentChild(Label)
  public labelRef: Label;

  @ContentChild(FormControlName)
  public formControlNameRef: FormControlName;

  private destroy$: EventEmitter<void>;

  constructor() {
    this.destroy$ = new EventEmitter<void>();
  }

  public ngAfterContentInit() {
    this.formControlNameRef.control.statusChanges
      .pipe(
        tap((status) => {
          const modifierColor = status === 'VALID' ? 'success' : 'danger';

          if (this.labelRef != null) {
            this.labelRef.color = modifierColor;
          }

          if (this.inputRef != null) {
            this.inputRef.color = modifierColor;
          }
        }),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  public ngOnDestroy() {
    this.destroy$.emit();
  }

}
