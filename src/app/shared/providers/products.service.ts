import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Product } from '../models/invoices.model';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient) { }

  public getProducts$() {
    const url = `${ environment.api }/products`;
    return this.http.get<Array<Product>>(url);
  }

  public getProduct$(id: string) {
    const url = `${ environment.api }/products/${ id }`;
    return this.http.get<Product>(url);
  }

  public createProduct$(Product: Product) {
    const url = `${ environment.api }/products`;
    return this.http.post<Product>(url, Product);
  }

  public updateProduct$(Product: Product) {
    const url = `${ environment.api }/products`;
    return this.http.put<Product>(url, Product);
  }

  public deleteProduct$(id: string) {
    const url = `${ environment.api }/products/${ id }`;
    return this.http.delete<null>(url);
  }
}
