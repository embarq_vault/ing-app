import { Injectable } from '@angular/core';
import { ValidatorFn, Validators } from '@angular/forms';

export interface ValidationRulesMap {
  [key: string]: Array<ValidatorFn>;
}

export interface ValidationMessagesMap {
  [key: string]: {
    [messageKey: string]: string
  };
}

@Injectable({
  providedIn: 'root'
})
export class ValidationService {
  private readonly validationRules: ValidationRulesMap;
  private readonly validationMessages: ValidationMessagesMap;

  constructor() {
    this.validationRules = {
      dob: [
        Validators.required
      ],
      email: [
        Validators.required,
        Validators.email
      ],
      password: [
        Validators.required,
        Validators.minLength(8),
        Validators.pattern(/[a-zA-Z0-9]+/)
      ],
      text: [
        Validators.required,
        Validators.pattern(/\w+/),
        Validators.minLength(3)
      ]
    };

    this.validationMessages = {
      common: {
        required: 'Field is required'
      },
      email: {
        email: 'Email is incorrect. E.g. example@gmail.com',
      },
      password: {
        minlength: 'Password must be 8 or more symbols',
        pattern: 'Password can only contain letters and numbers'
      },
      text: {
        minlength: 'Must be at least 3 characters length',
        pattern: 'Must contain only alpha characters'
      }
    };
  }

  public getRules(category: string) {
    return Validators.compose(this.validationRules[category]);
  }

  public getValidationMessages(validationType: string) {
    return {
      ...this.validationMessages.common,
      ...this.validationMessages[validationType]
    };
  }

  public getMinAllowedDOB() {
    const now = new Date(Date.now());
    return new Date(now.getFullYear() - 18, now.getMonth(), now.getDate());
  }
}
