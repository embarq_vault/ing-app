import { Injectable, EventEmitter } from '@angular/core';
import { LoadingController, ToastController, PickerController } from '@ionic/angular';
import { LoadingOptions, ToastOptions, PickerColumnOption } from '@ionic/core';
import { Product } from '../models/invoices.model';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UiService {
  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private pickerCtrl: PickerController
  ) { }

  public createLoading(opts: LoadingOptions = { message: 'Please wait' }) {
    return this.loadingCtrl.create(opts);
  }

  public presentSuccessToast(opts?: ToastOptions) {
    const options: ToastOptions = {
      message: 'Operation success',
      duration: 2500,
      showCloseButton: true,
      ...opts
    };

    return this.toastCtrl
      .create(options)
      .then(toast => {
        toast.present();
        return toast;
      });
  }

  public presentFailureToast(opts?: ToastOptions) {
    const options = {
      message: 'Operation failure, an error occured',
      showCloseButton: true,
      ...opts
    };
    return this.toastCtrl
      .create(options)
      .then(toast => {
        toast.present();
        return toast;
      });
  }

  public presentProductsPicker(products: Array<Product>) {
    const quantityColumnOptions: Array<PickerColumnOption> = [1,5,10,20,40,80,100].map(x => ({
      text: x + '',
      value: x
    }));
    const productsColumnOptions: Array<PickerColumnOption> = products.map(product => ({
      text: `${ product.name } - $${ product.price }`,
      value: product
    }));
    const result$ = new Subject();

    this.pickerCtrl
      .create({
        columns: [
          {
            name: 'product',
            columnWidth: '80%',
            selectedIndex: 0,
            cssClass: 'products-picker-column',
            options: productsColumnOptions
          },
          {
            name: 'quantity',
            selectedIndex: 0,
            columnWidth: '20%',
            options: quantityColumnOptions
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel'
          },
          {
            text: 'Add',
            role: 'Ok',
            handler: (value) => {
              const product = {
                product: value.product.value,
                quantity: value.quantity.value
              }
              result$.next(product);
              result$.complete();
            }
          }
        ]
      })
      .then(picker => picker.present());

    return result$;
  }
}
