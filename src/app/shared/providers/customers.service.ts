import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Customer } from '../models/invoices.model';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  constructor(private http: HttpClient) { }

  public getCustomers$() {
    const url = `${ environment.api }/customers`;
    return this.http.get<Array<Customer>>(url);
  }

  public getCustomer$(id: string) {
    const url = `${ environment.api }/customers/${ id }`;
    return this.http.get<Customer>(url);
  }

  public createCustomer$(customer: Customer) {
    const url = `${ environment.api }/customers`;
    return this.http.post<Customer>(url, customer);
  }

  public updateCustomer$(customer: Customer) {
    const url = `${ environment.api }/customers`;
    return this.http.put<Customer>(url, customer);
  }

  public deleteCustomer$(id: string) {
    const url = `${ environment.api }/customers/${ id }`;
    return this.http.delete<null>(url);
  }
}
