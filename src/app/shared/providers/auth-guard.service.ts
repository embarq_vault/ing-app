import { Injectable } from '@angular/core';
import { CanLoad, CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';
import { of } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthGuardService implements CanLoad, CanActivate {
    constructor(
        private auth: AuthService,
        private router: Router
    ) { }

    private handleNavigation() {
        if (this.auth.isAuthenticated()) {
            return of(true);
        }
        this.router.navigate(['/auth']);
    }

    public canLoad() {
        return this.handleNavigation();
    }

    public canActivate() {
        return this.handleNavigation();
    }
}
