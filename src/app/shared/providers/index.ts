import { AuthService } from './auth.service';
import { AuthGuardService } from './auth-guard.service';
import { ValidationService } from './validation.service';
import { UiService } from './ui.service';
import { ProductsService } from './products.service';
import { CustomersService } from './customers.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './auth-iterceptor.service';

export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
];

export const Providers = [
  ...httpInterceptorProviders,
  AuthService,
  AuthGuardService,
  ValidationService,
  UiService,
  ProductsService,
  CustomersService
];
