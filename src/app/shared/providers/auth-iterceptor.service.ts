import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpHeaders
} from '@angular/common/http';

import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private auth: AuthService) {}

  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.auth.getToken();

    if (this.auth.isAuthenticated && token != null && typeof token === 'string' && token.length > 0) {
      const headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': token
      });
      const authReq = request.clone({ headers });
      return next.handle(authReq);
    }

    return next.handle(request);
  }
}
