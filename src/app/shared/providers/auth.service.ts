import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

import { Observable } from 'rxjs';
import { tap, catchError, map } from 'rxjs/operators';
import { User, Credentials } from '../models/user.model';

export interface LoginResponse {
  id: string;
  ttl: string;
  created: string;
  userId: string;
}

export interface ErrorResponse {
  error: {
    statusCode: number;
    name: string;
    message: string;
    code: string;
  };
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _isAuthenticated: boolean;

  constructor(private http: HttpClient) {
    const userToken = localStorage.getItem('id');
    this._isAuthenticated = userToken != null;
  }

  public login(credentials: Credentials) {
    const url = `${environment.api}/ing-users/login`;
    return this.http
      .post<LoginResponse>(url, credentials)
      .pipe(
        tap(res => {
          for (const prop in res) {
            localStorage.setItem(prop, res[prop]);
          }
          this._isAuthenticated = true;
        }),
        catchError((err: ErrorResponse) => {
          this._isAuthenticated = false;
          return Observable.throw(err);
        })
      );
  }

  public register(user: User) {
    const url = `${ environment.api }/ing-users`;
    return this.http.post<User>(url, user);
  }

  public isAuthenticated() {
    return this._isAuthenticated;
  }

  public getToken() {
    return localStorage.getItem('id');
  }

  public getUserId() {
    return localStorage.getItem('userId');
  }

  public logout() {
    const url = `${ environment.api }/ing-users/logout`;
    const requestOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `${ this.getToken() }`
      })
    };

    return this.http
      .post<null>(url, null, requestOptions)
      .pipe(
        tap(() => {
          const dataTokens = ['id', 'ttl', 'userId'];

          for (let prop of dataTokens) {
            localStorage.removeItem(prop);
          }

          this._isAuthenticated = false;
        })
      )
  }
}
