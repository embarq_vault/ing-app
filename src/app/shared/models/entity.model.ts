export interface TrackedEntity {
  id?: string;
  createdAt?: Date | string;
  updatedAt?: Date | string;
}
