import { TrackedEntity } from "./entity.model";

export interface Invoice extends TrackedEntity {
  total: number;
  customerId: string;
  discount: number;
  customer?: Customer;
}

export interface InvoiceItem extends TrackedEntity {
  quantity: number;
  invoiceId: string;
  productId: string;
}

export interface Product extends TrackedEntity {
  name: string;
  price: number;
  currency?: string;
}

export interface Customer extends TrackedEntity {
  name: string;
  phone: string;
  address: string;
}
