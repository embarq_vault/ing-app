export interface Credentials {
  email: string;
  password: string;
}

export interface User {
  name: string;
  email: string;
  dob?: string;
  username?: string;
  password?: string;
  id?: string;
  photo?: string;
  realm?: string;
  emailVerified?: boolean;
}
