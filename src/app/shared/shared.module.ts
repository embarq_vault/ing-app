import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { ValidationErrorComponent } from './validation-error/validation-error.component';
import { IonicModule } from '@ionic/angular';
import { ValidationUiAdapterDirective } from './validation-ui-adapter/validation-ui-adapter.directive';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule
  ],
  declarations: [
    ValidationErrorComponent,
    ValidationUiAdapterDirective
  ],
  exports: [
    ValidationErrorComponent,
    ValidationUiAdapterDirective
  ]
})
export class SharedModule { }
