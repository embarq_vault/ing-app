import { Component, OnInit, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { ValidationService } from '../../shared/providers/validation.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'validation-error',
  templateUrl: './validation-error.component.html',
  styleUrls: ['./validation-error.component.scss']
})
export class ValidationErrorComponent implements OnInit {

  @Input('category')
  public validationType: string;

  @Input()
  public control: AbstractControl;

  public stateMessages$: Observable<Array<string>>;
  public validationMessages: { [key: string]: string };

  constructor(
    private validationService: ValidationService
  ) {
    this.validationMessages = null;
  }

  public getValidationMessages(): Array<string> {
    console.log(this.control.errors);
    return Object
      .keys(this.control.errors)
      .map(errorKey => this.validationMessages[errorKey]);
  }

  public ngOnInit() {
    this.validationMessages = this.validationService.getValidationMessages(this.validationType);
    this.stateMessages$ = this.control.statusChanges
      .pipe(
        map((status) => {
          return status === 'INVALID' ? this.getValidationMessages() : [];
        })
      );
  }

}
