import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ing-auth',
  template: `<ion-router-outlet></ion-router-outlet>`
})
export class AuthPage { }
