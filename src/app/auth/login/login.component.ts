import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { tap, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { ValidationService } from '../../shared/providers/validation.service';
import { AuthService } from '../../shared/providers/auth.service';
import { UiService } from '../../shared/providers/ui.service';

@Component({
  selector: 'ing-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;

  constructor(
    private validationService: ValidationService,
    private authProvider: AuthService,
    private uiService: UiService,
    private router: Router
  ) {
    this.loginForm = null;
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl(null, this.validationService.getRules('email')),
      password: new FormControl(null, this.validationService.getRules('password'))
    }, {
      updateOn: 'blur'
    });
  }

  private markFieldsAsDirty() {
    for (let control in this.loginForm.controls) {
      this.loginForm.controls[control].markAsDirty();
      this.loginForm.controls[control].updateValueAndValidity({
        emitEvent: true
      });
    }
  }

  public async handleSubmit() {
    if (this.loginForm.invalid) {
      await this.uiService.presentFailureToast({
        message: 'Please check the data you have entered'
      });
      return this.markFieldsAsDirty();
    }

    const loading = await this.uiService.createLoading()
    await loading.present();

    this.authProvider
      .login(this.loginForm.value)
      .pipe(
        tap(async () => {
          await loading.dismiss();
          await this.uiService.presentSuccessToast();
          await this.router.navigate(['/invoices']);
        }),
        catchError(async (err) => {
          await loading.dismiss();
          await this.uiService.presentFailureToast({
            message: 'Please check the data you have entered'
          });
          console.log(err);
          return Observable.throw(err)
        })
      )
      .subscribe();
  }
}
