import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { tap, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { ValidationService } from '../../shared/providers/validation.service';
import { AuthService } from '../../shared/providers/auth.service';
import { UiService } from '../../shared/providers/ui.service';

@Component({
  selector: 'ing-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public readonly minAllowedDate: Date;

  public registerForm: FormGroup;

  constructor(
    private validationService: ValidationService,
    private authProvider: AuthService,
    private uiService: UiService,
    private router: Router
  ) {
    this.minAllowedDate = this.validationService.getMinAllowedDOB();
  }

  ngOnInit() {
    this.registerForm = new FormGroup({
      name: new FormControl(null, this.validationService.getRules('text')),
      email: new FormControl(null, this.validationService.getRules('email')),
      password: new FormControl(null, this.validationService.getRules('password')),
      dob: new FormControl(null, {
        validators: this.validationService.getRules('dob'),
        updateOn: 'change'
      })
    }, {
      updateOn: 'blur'
    });
  }

  private markFieldsAsDirty() {
    for (let control in this.registerForm.controls) {
      this.registerForm.controls[control].markAsDirty();
      this.registerForm.controls[control].updateValueAndValidity({
        emitEvent: true
      });
    }
  }

  public async handleSubmit() {
    if (this.registerForm.invalid) {
      await this.uiService.presentFailureToast({
        message: 'Please check the data you have entered'
      });
      return this.markFieldsAsDirty();
    }

    const loading = await this.uiService.createLoading()
    await loading.present();

    this.authProvider
      .register(this.registerForm.value)
      .pipe(
        tap(async () => {
          await loading.dismiss();
          await this.uiService.presentSuccessToast();
          await this.router.navigate(['/auth/login']);
        }),
        catchError(async (err) => {
          await loading.dismiss();
          await this.uiService.presentFailureToast({
            message: 'Please check the data you have entered'
          });
          console.log(err);
          return Observable.throw(err)
        })
      )
      .subscribe();
  }

}
