import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { SharedModule } from '../shared/shared.module';

import { CustomersRoutingModule } from './customers-routing.module';
import { CustomersComponent } from './customers/customers.component';
import { CustomersEditorComponent } from './customers-editor/customers-editor.component';
import { CustomersListComponent } from './customers-list/customers-list.component';

@NgModule({
  imports: [
    CommonModule,
    CustomersRoutingModule,
    IonicModule,
    SharedModule,
    ReactiveFormsModule
  ],
  declarations: [
    CustomersComponent,
    CustomersEditorComponent,
    CustomersListComponent
  ]
})
export class CustomersModule { }
