import { Component, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Customer } from '../../shared/models/invoices.model';
import { CustomersService } from '../../shared/providers/customers.service';
import { ActivatedRoute } from '@angular/router';
import { takeUntil, tap, catchError, switchMap, map, delay, concatAll, withLatestFrom, concat, merge } from 'rxjs/operators';
import { UiService } from '../../shared/providers/ui.service';

@Component({
  selector: 'app-customers-list',
  templateUrl: './customers-list.component.html',
  styleUrls: ['./customers-list.component.scss']
})
export class CustomersListComponent implements OnInit, OnDestroy {
  private readonly destroy$: EventEmitter<void>;
  public readonly customerRemove$: EventEmitter<string>;
  public customers$: Observable<Array<Customer>>;

  constructor(
    private customersService: CustomersService,
    private route: ActivatedRoute,
    private uiService: UiService
  ) {
    this.destroy$ = new EventEmitter<void>();
    this.customerRemove$ = new EventEmitter<string>();
  }

  public ngOnInit() {
    const customers$ = this.customersService.getCustomers$().pipe(tap((data) => console.log('customers$', data)));

    console.log('init');

    const refresh$ = this.route.queryParams
      .pipe(
        tap((data) => console.log('refresh$', data)),
        switchMap((queryParams) => {
          if ('refresh' in queryParams) {
            return this.customersService.getCustomers$()
              .pipe(tap((data) => console.log('refresh$: fetched', data)))
          }
          return of([]).pipe(withLatestFrom(customers$));
        })
      );

    const customerRemove$ = this.customerRemove$.pipe(
      tap((data) => console.log('customerRemove$', data)),
      delay(300),
      withLatestFrom(customers$),
      map(([customerId, customers]) => customers.filter(customer => customer.id !== customerId)),
      tap((data) => console.log('customerRemove$', data))
    );

    this.customers$ = customers$
      .pipe(
        merge(refresh$, customerRemove$)
      );
  }

  public ngOnDestroy() {
    this.destroy$.emit();
  }

  public async removeCustomer(customerId: string) {
    const loader = await this.uiService.createLoading();
    loader.present();

    this.customersService
      .deleteCustomer$(customerId)
      .pipe(
        tap(() => {
          loader.dismiss();
          this.uiService.presentSuccessToast({
            message: 'Customer successfully removed'
          });
          this.customerRemove$.emit(customerId)
        }),
        catchError((err) => {
          loader.dismiss();
          this.uiService.presentFailureToast({
            message: 'An error occured while removing customer'
          });
          return Observable.throw(err);
        })
      )
      .subscribe();
  }

}
