import { Component, OnInit } from '@angular/core';
import { CustomersService } from '../../shared/providers/customers.service';
import { FormGroup, FormControl } from '@angular/forms';
import { ValidationService } from '../../shared/providers/validation.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { UiService } from '../../shared/providers/ui.service';
import { tap, catchError, filter, switchMap } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-customers-editor',
  templateUrl: './customers-editor.component.html',
  styleUrls: ['./customers-editor.component.scss']
})
export class CustomersEditorComponent implements OnInit {
  public readonly customerForm: FormGroup;
  public readonly loading$: BehaviorSubject<boolean>;
  public isEdit: boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private customersService: CustomersService,
    private validationService: ValidationService,
    private uiService: UiService
  ) {
    this.loading$ = new BehaviorSubject<boolean>(false);
    this.customerForm = new FormGroup({
      name: new FormControl(null, this.validationService.getRules('text')),
      phone: new FormControl(null, this.validationService.getRules('text')),
      address: new FormControl(null, this.validationService.getRules('text'))
    }, {
      updateOn: 'blur'
    });
    this.isEdit = false;
  }

  private markFieldsAsDirty() {
    for (let control in this.customerForm.controls) {
      this.customerForm.controls[control].markAsDirty();
      this.customerForm.controls[control].updateValueAndValidity({
        emitEvent: true
      });
    }
  }

  public ngOnInit() {
    this.route.params
      .pipe(
        filter(params => 'id' in params && params.id != null),
        tap(() => this.isEdit = true),
        switchMap(params => this.customersService.getCustomer$(params.id)),
        tap(customer => this.customerForm.patchValue(customer))
      )
      .subscribe()
  }

  public save() {
    if (this.customerForm.invalid) {
      this.markFieldsAsDirty();
      return;
    }

    this.loading$.next(true);

    this.customersService
      .createCustomer$(this.customerForm.value)
      .pipe(
        tap(async () => {
          this.loading$.next(false);
          await this.uiService.presentSuccessToast({
            message: 'Customer successfully created'
          });
          this.router.navigate(['/customers/list'], { queryParams: { refresh: true } });
        }),
        catchError(async (err) => {
          this.loading$.next(false);
          await this.uiService.presentFailureToast({
            message: 'Unable to create customer. Please check the data you have entered'
          });
          console.log(err);
          return Observable.throw(err)
        })
      )
      .subscribe();
  }

  public update() {
    if (this.customerForm.invalid) {
      this.markFieldsAsDirty();
      return;
    }

    this.loading$.next(true);

    this.customersService
      .updateCustomer$(this.customerForm.value)
      .pipe(
        tap(async () => {
          this.loading$.next(false);
          await this.uiService.presentSuccessToast({
            message: 'Customer successfully updated'
          });
          this.router.navigate(['/customers/list'], { queryParams: { refresh: true } });
        }),
        catchError(async (err) => {
          this.loading$.next(false);
          await this.uiService.presentFailureToast({
            message: 'Unable to update customer\'s info\. Please check the data you have entered'
          });
          console.log(err);
          return Observable.throw(err)
        })
      )
      .subscribe();
  }

}
