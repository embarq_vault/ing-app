import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomersEditorComponent } from './customers-editor.component';

describe('CustomersEditorComponent', () => {
  let component: CustomersEditorComponent;
  let fixture: ComponentFixture<CustomersEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomersEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomersEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
