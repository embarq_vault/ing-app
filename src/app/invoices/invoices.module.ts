import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { InvoicesRoutingModule } from './invoices-routing.module';
import { InvoicesListComponent } from './invoices-list/invoices-list.component';
import { InvoicesEditorComponent } from './invoices-editor/invoices-editor.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoicesService } from './invoices.service';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    InvoicesRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [
    InvoicesComponent,
    InvoicesListComponent,
    InvoicesEditorComponent
  ],
  providers: [
    InvoicesService
  ]
})
export class InvoicesModule { }
