import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ing-invoices',
  template: `<ion-router-outlet></ion-router-outlet>`,
})
export class InvoicesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
