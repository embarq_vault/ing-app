import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoicesListComponent } from './invoices-list/invoices-list.component';
import { InvoicesEditorComponent } from './invoices-editor/invoices-editor.component';

const routes: Routes = [
  {
    path: '',
    component: InvoicesComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list'
      },
      {
        path: 'list',
        component: InvoicesListComponent
      },
      {
        path: 'editor',
        component: InvoicesEditorComponent
      },
      {
        path: 'editor/:id',
        component: InvoicesEditorComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoicesRoutingModule { }
