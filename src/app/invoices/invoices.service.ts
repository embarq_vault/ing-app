import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { of, forkJoin, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AuthService } from '../shared/providers/auth.service';
import { environment } from '../../environments/environment';
import { Invoice, InvoiceItem } from '../shared/models/invoices.model';
import { ProductsService } from '../shared/providers/products.service';

@Injectable({
  providedIn: 'root'
})
export class InvoicesService {

  constructor(
    private authProvider: AuthService,
    private http: HttpClient,
    private products: ProductsService
  ) { }

  public getInvoices$() {
    const userId = this.authProvider.getUserId();
    const url = `${ environment.api }/invoices?filter[createdBy]=${ userId }&filter[include]=customer`;
    return this.http.get<Array<Invoice>>(url);
  }

  public getInvoice$(invoiceId: string) {
    if (invoiceId == null) {
      return of(null);
    }

    const invoiceUrl = `${ environment.api }/invoices/${ invoiceId }`;
    const invoiceItemsUrl = `${ environment.api }/invoices/${ invoiceId }/items`;

    return forkJoin(
      this.http.get<Invoice>(invoiceUrl),
      this.http.get<Array<InvoiceItem>>(invoiceItemsUrl),
      this.products.getProducts$()
    )
    .pipe(
      map(([ invoice, items, products ]) => {
        const productsMap = products.reduce((_map, product) => {
          _map[product.id] = product;
          return _map;
        }, {});

        const invoiceItems = items.map(item => {
          return {
            ...item,
            product: productsMap[item.productId]
          };
        });

        return {
          invoice: invoice,
          items: invoiceItems
        };
      })
    );
  }

  public createInvoice$(invoice: Invoice): Observable<any> {
    const url = `${ environment.api }/invoices`;
    const userId = this.authProvider.getUserId();
    const _invoice = { ...invoice, createdById: userId };
    return this.http.post(url, _invoice);
  }

  public updateInvoice$(invoice: Invoice): Observable<any> {
    const url = `${ environment.api }/invoices/${ invoice.id }`;
    return this.http.put(url, invoice);
  }

  public upsertInvoiceItems$(invoiceId: string, products: Array<any>): Observable<any[]> {
    const url = `${ environment.api }/invoices/${ invoiceId }/items/`;
    const invoiceItems$ = products.map(entry => {
      const item: InvoiceItem = {
        invoiceId: invoiceId,
        quantity: entry.quantity,
        productId: entry.product.id
      };
      if (entry.id != null) {
        return this.http.put(url + entry.id, item);
      }
      return this.http.post(url, item);
    });

    return forkJoin(...invoiceItems$);
  }

  public removeItem$(invoiceId, itemId) {
    const url = `${ environment.api }/invoices/${ invoiceId }/items/${ itemId }`;
    return this.http.delete(url);
  }
}
