import { Component, OnInit, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Observable, merge, BehaviorSubject, of } from 'rxjs';
import { Product, Customer } from '../../shared/models/invoices.model';
import { InvoicesService } from '../invoices.service';
import { shareReplay, switchMap, tap, debounceTime, filter, map, catchError } from 'rxjs/operators';
import { UiService } from '../../shared/providers/ui.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Content } from '@ionic/angular';
import { ProductsService } from '../../shared/providers/products.service';
import { CustomersService } from '../../shared/providers/customers.service';

@Component({
  selector: 'ing-invoices-editor',
  templateUrl: './invoices-editor.component.html',
  styleUrls: ['./invoices-editor.component.scss']
})
export class InvoicesEditorComponent implements OnInit {
  public invoiceFormGroup: FormGroup;
  public isEdit: boolean;

  public get invoiceItemsGroup() {
    return this.invoiceFormGroup.get('items') as FormArray;
  }

  public products$: Observable<Array<Product>>;
  public customers$: Observable<Array<Customer>>;
  public loading$: BehaviorSubject<boolean>;
  private onItemRemove$: EventEmitter<void>;

  constructor(
    private uiService: UiService,
    private invoicesService: InvoicesService,
    private productsService: ProductsService,
    private customersService: CustomersService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.isEdit = false;
    this.loading$ = new BehaviorSubject<boolean>(false);
    this.onItemRemove$ = new EventEmitter<void>();
    this.products$ = this.productsService
      .getProducts$()
      .pipe(shareReplay(1));
    this.customers$ = this.customersService
      .getCustomers$()
      .pipe(shareReplay(1));
  }

  private handleProductAdd(entry, isHydration = false) {
    const productGroup = new FormGroup({
      id: new FormControl(entry.id || null),
      product: new FormControl(entry.product),
      quantity: new FormControl(entry.quantity, {
        validators: Validators.min(1),
        updateOn: 'blur'
      })
    });

    if (!isHydration) {
      productGroup.markAsDirty();
    }

    this.invoiceItemsGroup.push(productGroup);
    this.invoiceItemsGroup.updateValueAndValidity({
      emitEvent: true
    });
    this.invoiceFormGroup.markAsDirty();
  }

  private getAutoUpdate$() {
    return this.invoiceFormGroup.valueChanges
      .pipe(
        debounceTime(200),
        tap(() => console.log('Running auto-update')),
        filter(() => this.invoiceFormGroup.dirty && this.invoiceFormGroup.valid),
        tap(() => this.loading$.next(true)),
        switchMap(() => {
          const { id, total, discount, customerId } = this.invoiceFormGroup.value;
          return this.invoicesService.updateInvoice$({ id, total, discount, customerId });
        }),
        switchMap(
          invoice => {
            const modifiedItems = this.invoiceItemsGroup.controls
              .filter(control => control.dirty && control.valid)
              .map(control => control.value);

            if (modifiedItems.length < 1) {
              return of(this.invoiceItemsGroup.value);
            }

            return this.invoicesService.upsertInvoiceItems$(invoice.id, modifiedItems);
          },
          (invoice, items) => ({ invoice, items })
        ),
        tap(async () => {
          this.loading$.next(false);
          await this.uiService.presentSuccessToast({
            message: 'Invoice successfully updated'
          });
        }),
        tap((result) => {
          const updateOpts = {
            onlySelf: true,
            emitEvent: false
          }

          if (this.invoiceFormGroup.get('id').value == null) {
            this.invoiceFormGroup.get('id').setValue(result.invoice.id, updateOpts);
          }

          result.items.forEach((item, index) => {
            const itemGroup = this.invoiceItemsGroup.at(index);
            if (itemGroup.get('id').value == null) {
              itemGroup.get('id').setValue(item.id, updateOpts);
            }
          });
        }),
        catchError(async (err) => {
          this.loading$.next(false);
          await this.uiService.presentFailureToast({
            message: 'Unable to update invoice. Please check the data you have entered'
          });
          return Observable.throw(err);
        })
      )
  }

  private getTotal$() {
    return merge(
      this.invoiceItemsGroup.valueChanges
        .pipe(filter(() => this.invoiceItemsGroup.valid)),
      this.invoiceFormGroup.get('discount').valueChanges
        .pipe(filter(() => this.invoiceFormGroup.get('discount').valid)),
      this.onItemRemove$.asObservable()
    )
    .pipe(
      map(() => {
        const discount = Number(this.invoiceFormGroup.get('discount').value) / 100;
        const total = this.invoiceItemsGroup.value
          .map(({ product, quantity }) => product.price * quantity)
          .reduce((total, subtotal) => total + subtotal, 0);
        return Number((total - discount * total).toFixed(2));
      })
    )
  }

  public ngOnInit() {
    const invoiceFormGroupControls = {
      id: new FormControl(null),
      createdById: new FormControl(null),
      customerId: new FormControl(null, {
        validators: Validators.required,
        updateOn: 'change'
      }),
      discount: new FormControl(0, Validators.min(0)),
      total: new FormControl(0, {
        updateOn: 'change'
      }),
      items: new FormArray([ ], (group: FormArray) => {
        if (group.length < 1) {
          return { itemsRequired: true }
        }
        return null
      })
    };

    this.invoiceFormGroup = new FormGroup(invoiceFormGroupControls, {
      updateOn: 'blur'
    });

    const edit$ = this.route.params
      .pipe(
        filter(params => 'id' in params && params.id != null),
        tap(() => this.isEdit = true),
        switchMap((params) => this.invoicesService.getInvoice$(params.id)),
        tap(({ invoice, items }) => {
          this.invoiceFormGroup.patchValue(invoice);
          items.forEach(item => this.handleProductAdd(item, true));
        }),
        switchMap(() => this.getAutoUpdate$())
      );
    const totalUpdate$ = this.getTotal$()
      .pipe(
        tap(total => {
          this.invoiceFormGroup.get('total').setValue(total);
        })
      );

    merge(edit$, totalUpdate$).subscribe()
  }

  public pickProduct() {
    this.products$
      .pipe(
        switchMap(products => this.uiService.presentProductsPicker(products)),
        tap(selectedProduct => this.handleProductAdd(selectedProduct))
      )
      .subscribe()
  }

  public async save() {
    this.loading$.next(true);

    this.invoicesService
      .createInvoice$(this.invoiceFormGroup.value)
      .pipe(
        switchMap(
          invoice => this.invoicesService.upsertInvoiceItems$(invoice.id, this.invoiceItemsGroup.value),
          (invoice, items) => ({ invoice, items })
        ),
        tap(async () => {
          this.loading$.next(false);
          await this.uiService.presentSuccessToast({
            message: 'Invoice successfully created'
          });
          this.router.navigate(['/invoices'], { queryParams: { refresh: true } });
        }),
        catchError(async (err) => {
          this.loading$.next(false);
          await this.uiService.presentFailureToast({
            message: 'Unable to create invoice. Please check the data you have entered'
          });
          console.log(err);
          return Observable.throw(err)
        })
      )
      .subscribe();
  }

}
