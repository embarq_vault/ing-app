import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicesEditorComponent } from './invoices-editor.component';

describe('InvoicesEditorComponent', () => {
  let component: InvoicesEditorComponent;
  let fixture: ComponentFixture<InvoicesEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoicesEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicesEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
