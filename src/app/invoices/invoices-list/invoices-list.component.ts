import { Component, OnInit } from '@angular/core';
import { InvoicesService } from '../invoices.service';
import { Observable } from 'rxjs';
import { Invoice } from '../../shared/models/invoices.model';
import { ActivatedRoute } from '@angular/router';
import { filter, tap } from 'rxjs/operators';

@Component({
  selector: 'ing-invoices-list',
  templateUrl: './invoices-list.component.html',
  styleUrls: ['./invoices-list.component.scss']
})
export class InvoicesListComponent implements OnInit {
  public invoices$: Observable<Array<Invoice>>;

  constructor(
    private invoicesService: InvoicesService,
    private route: ActivatedRoute
  ) { }

  public ngOnInit() {
    this.invoices$ = this.invoicesService.getInvoices$();
    this.route.queryParams
      .pipe(
        filter(queryParams => 'refresh' in queryParams),
        tap(() => {
          this.invoices$ = this.invoicesService.getInvoices$();
        })
      )
      .subscribe()
  }

}
